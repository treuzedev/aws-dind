# aws-dind

AWS CLI + DinD for Amazon Linux 2

<br>

## As a container

`docker run -v /var/run/docker.sock:/var/run/docker.sock 63a19046a2a8 docker run hello-world`

<br>

## In a pipeline

```
test:
  stage: 'test'
  image: 'registry.gitlab.com/treuzedev/aws-dind:0.1.1'
  services:
    - 'docker:dind'
  variables:
    DOCKER_HOST: 'tcp://docker:2375/'
    DOCKER_DRIVER: 'overlay'
  tags:
    - 'gitlab-org-docker'
  script:
    - 'docker info'
```
